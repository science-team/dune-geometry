dune-geometry (2.10.0-1) unstable; urgency=medium

  * New upstream version 2.10.0
  * Added debian/watch file
  * Bumped standards version to 4.7.0 (no changes)
  * d/control: Depend on pkgconf and DUNE 2.10~
  * d/patches: Refreshed patches

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 14 Nov 2024 19:01:13 +0100

dune-geometry (2.10~pre20240905-1) experimental; urgency=medium

  * New upstream release (2.10~pre20240905).
  * Bumped standards version to 4.7.0 (no changes)
  * d/control: Depend on pkgconf and DUNE 2.10~
  * d/patches: Refreshed patches

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 17 Oct 2024 20:16:54 +0200

dune-geometry (2.9.0-4) unstable; urgency=medium

  * Use fuzzy check for volume of reference elements to fix
      failing tests on i386

 -- Markus Blatt <markus@dr-blatt.de>  Sat, 15 Jul 2023 16:03:47 +0200

dune-geometry (2.9.0-3) unstable; urgency=medium

  * d/patches: Backported patch from upstream to fix compilation with GCC13
      (Closes: #1037631, #1037632, #1037633, #1037634)

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 13 Jul 2023 13:14:05 +0200

dune-geometry (2.9.0-2) unstable; urgency=medium

  * d/control: Added Markus Blatt as uploader (with consent of Ansgar)
  * Upload to unstable

 -- Markus Blatt <markus@dr-blatt.de>  Thu, 12 Jan 2023 18:09:23 +0100

dune-geometry (2.9.0-1) experimental; urgency=medium

  [ Markus Blatt ]
  * d/upstream: Added metadata file
  * Fixed version number in dune.module
  * New upstream release (2.9.0).

  [ Ansgar ]
  * d/control: set Standards-Version to 4.6.2 (no changes).

 -- Ansgar <ansgar@debian.org>  Sat, 07 Jan 2023 22:13:32 +0100

dune-geometry (2.8.0-2) unstable; urgency=medium

  * Upload to unstable

 --  Patrick Jaap <patrick.jaap@tu-dresden.de>  Thu, 21 Oct 2021 18:36:45 +0200

dune-geometry (2.8.0-1) experimental; urgency=medium

  * New upstream release.

 --  Patrick Jaap <patrick.jaap@tu-dresden.de>  Wed, 15 Sep 2021 11:15:26 +0200

dune-geometry (2.8.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * debian/rules: Explicitly set buildsystem to CMake

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Fri, 20 Aug 2021 11:23:47 -0400

dune-geometry (2.7.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Mon, 11 Jan 2021 22:31:14 +0100

dune-geometry (2.7.1-1) experimental; urgency=medium

  * New upstream release.

 -- Lisa Julia Nebel  <lisa_julia.nebel@tu-dresden.de>  Thu, 07 Jan 2021 09:35:42 +0100

dune-geometry (2.7.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Wed, 15 Jul 2020 12:37:31 +0200

dune-geometry (2.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Use debhelper compat level 13.
  * Bumped Standards-Version to 4.5.0 (no changes).

 -- Ansgar <ansgar@debian.org>  Sun, 24 May 2020 16:49:42 +0200

dune-geometry (2.6.0-1) unstable; urgency=medium

  * New upstream release.
  * d/control: now requires cmake >= 3.1

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 03 Apr 2018 00:30:47 +0200

dune-geometry (2.6.0~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * add Build-Depends-Indep: texlive-latex-extra for type1cm.sty
  * add Build-Depends-Indep: inkscape for SVG to EPS conversion
  * d/control: update Vcs-* fields for move to salsa.debian.org
  * Bumped Standards-Version to 4.1.3 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 06 Jan 2018 19:16:43 +0100

dune-geometry (2.6~20171113-1) experimental; urgency=medium

  * New upstream snapshot.
  * d/control: add `Rules-Requires-Root: no`
  * libdune-geometry-doc: add Built-Using: doxygen (for jquery.js)
  * Bumped Standards-Version to 4.1.1 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 14 Nov 2017 00:18:11 +0100

dune-geometry (2.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Build-Depend on texlive-pictures instead of pgf. (Closes: #867075)
  * debian/copyright: Update URLs.
  * Bumped Standards-Version to 4.0.0 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 18 Jul 2017 11:55:38 +0200

dune-geometry (2.5.1~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * No longer build manual -dbg package. Use the automatically generated
    -dbgsym package instead.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 17 Jun 2017 16:52:00 +0200

dune-geometry (2.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 18 Dec 2016 13:20:47 +0100

dune-geometry (2.5.0~rc2-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release candidate.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 05 Dec 2016 23:23:32 +0100

dune-geometry (2.5.0~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last DUNE upload.
  * Switch to CMake.
  * Bumped Standards-Version to 3.9.8 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 21 Nov 2016 20:37:20 +0100

dune-geometry (2.4.1-1) unstable; urgency=medium

  * New upstream release.
  * (Build-)Depend on last dune-common upload.
  * Mark libdune-geometry-dev as Multi-Arch: same.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 29 Feb 2016 10:23:32 +0100

dune-geometry (2.4.1~rc2-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last dune-common upload.
  * debian/control: Update Vcs-* fields.
  * Mark libdune-geometry-doc as Multi-Arch: foreign.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 28 Feb 2016 13:53:02 +0100

dune-geometry (2.4.0-1) unstable; urgency=medium

  * New upstream release.
  * (Build-)Depend on last dune-common upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 25 Sep 2015 22:31:18 +0200

dune-geometry (2.4~20150912rc3-1) unstable; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last dune-common upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 13 Sep 2015 13:46:12 +0200

dune-geometry (2.4~20150825rc2-1) experimental; urgency=medium

  * New upstream release candidate.
  * Move shared library into -dev package and provide a virtual package
    that changes with the upstream version for shlib dependencies. See
    also https://lists.debian.org/debian-devel/2015/07/msg00115.html
  * (Build-)Depend on last dune-common upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 04 Sep 2015 15:37:22 +0200

dune-geometry (2.4~20150717rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * (Build-)Depend on last dune-common upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 17 Jul 2015 21:00:24 +0200

dune-geometry (2.4~20150616g82cf4be-1) experimental; urgency=medium

  * New upstream snapshot.
  * libdune-geometry-doc: Install documentation to correct location.
  * (Build-)Depend on last dune-common upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 19 Jun 2015 23:33:04 +0200

dune-geometry (2.4~20150505g14682fb-1) experimental; urgency=medium

  * New upstream snapshot.
  * Change shared library package name to libdune-geometry-2.4git.
  * Update debian/copyright for new upstream snapshot.
  * (Build-)Depend on last dune-common upload.
  * Add Build-Depends-Indep: graphviz for "dot".
  * Bumped Standards-Version to 3.9.6.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 05 May 2015 22:07:55 +0200

dune-geometry (2.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 17 Jun 2014 19:53:54 +0200

dune-geometry (2.3.1~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * Change shared library package name to libdune-geometry-2.3.1.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 10 Jun 2014 20:42:19 +0200

dune-geometry (2.3.0-2) unstable; urgency=medium

  * Install /usr/share/dune-geometry.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 14 Feb 2014 10:48:41 +0100

dune-geometry (2.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Change shared library package name to libdune-geometry-2.3.0.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 14 Feb 2014 09:27:28 +0100

dune-geometry (2.3~20140117beta2-1) experimental; urgency=medium

  * New upstream snapshot.
  * libdune-geometry-dev.install: Install /usr/lib/*/cmake.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 20 Jan 2014 21:20:35 +0100

dune-geometry (2.3~20140110beta1-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 15 Jan 2014 13:41:04 +0100

dune-geometry (2.3~20131228g31cd66f-2) experimental; urgency=medium

  * Increase epsilon for floating point comparisons in test-affinegeometry
    and test-multilineargeometry. This should address the build failure on
    powerpc, ppc64 and s390x.
    + new patch: 0001-Increase-epsilon-for-floating-point-comparisons.patch
  * Increase alignment of BasicGeometry::mappingStorage_. This should
    address the build failure on mips and sparc.
    + new patch: 0001-Fix-alignment-of-mappingStorage_-member.patch

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 04 Jan 2014 00:17:17 +0100

dune-geometry (2.3~20131228g31cd66f-1) experimental; urgency=medium

  * New upstream snapshot.
  * Change shared library package name to libdune-geometry-2.3svn.
  * Drop pre-depends on dpkg (>= 1.15.6) which is already satisfied in
    Debian 6.
  * debian/control: Use canonical Vcs-* URIs.
  * Bumped Standards-Version to 3.9.5 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 01 Jan 2014 19:54:54 +0100

dune-geometry (2.2.1-2) unstable; urgency=low

  * Upload to unstable.
  * Bumped Standards-Version to 3.9.4 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 18 May 2013 13:36:20 +0200

dune-geometry (2.2.1-1) experimental; urgency=low

  * New upstream release.
  * Update soname.
  * Mark runtime and debug packages as Multi-Arch: same.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 02 Mar 2013 00:34:47 +0100

dune-geometry (2.2.0-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 05 Jun 2012 10:26:28 +0200

dune-geometry (2.2~svn70-1) experimental; urgency=low

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 06 May 2012 15:08:15 +0200

dune-geometry (2.2~svn57-1) experimental; urgency=low

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 05 Apr 2012 20:42:57 +0200

dune-geometry (2.2~svn54-1) experimental; urgency=low

  * Initial release. (Closes: #661823)

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 29 Mar 2012 19:40:26 +0200
